// MAI Cup 2022 Senior Team
// Copyright (C) 2021  MAI Robotics
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <Arduino.h>
#include <QTRSensors.h>
#include <SoftwareI2C.h>
#include "../lib/VL53L0X/src/Adafruit_VL53L0X.h"

#include "pins.h"
#include "macros.h"
#include "timers.h"

// Disable the warning for the unknown parameters
// Don't format these lines since CLion wants to space the - character
//@formatter:off
DISABLE_WARNING_PUSH
DISABLE_WARNING(-Wunknown-attributes)
//@formatter:on

Adafruit_VL53L0X lox0 = Adafruit_VL53L0X();
SoftwareI2C sensor0I2C;

Adafruit_VL53L0X lox1 = Adafruit_VL53L0X();
SoftwareI2C sensor1I2C;

Adafruit_VL53L0X lox2 = Adafruit_VL53L0X();
SoftwareI2C sensor2I2C;

QTRSensors qtr;

const uint8_t SensorCount = 8;
uint16_t sensorValues[SensorCount];

ISR(TIMER3_OVF_vect) {
    SBI(STEP_LEFT_PIN_REG, STEP_LEFT_PIN);
    SBI(STEP_RIGHT_PIN_REG, STEP_RIGHT_PIN);
}

volatile uint16_t sensorData[3];

void readLidarSensors() {
    sensorData[0] = lox0.readRangeResult();
    sensorData[1] = lox1.readRangeResult();
    sensorData[2] = lox2.readRangeResult();
}

FORCE_INLINE void setupSensorArray() {
    delay(500);
    digitalWrite(LED_BUILTIN, HIGH); // turn on Arduino's LED to indicate we are in calibration mode
    // 2.5 ms RC read timeout (default) * 10 reads per calibrate() call
    // = ~25 ms per calibrate() call.
    // Call calibrate() 400 times to make calibration take about 10 seconds.
    for (uint16_t i = 0; i < 400; i++) {
        qtr.calibrate();
    }
    digitalWrite(LED_BUILTIN, LOW); // turn off Arduino's LED to indicate we are through with calibration

    for (uint8_t i = 0; i < SensorCount; i++) {
        DEBUG_PRINT(qtr.calibrationOn.minimum[i]);
        DEBUG_PRINT(' ');
    }
    DEBUG_PRINTLN();

    // print the calibration maximum values measured when emitters were on
    for (uint8_t i = 0; i < SensorCount; i++) {
        DEBUG_PRINT(qtr.calibrationOn.maximum[i]);
        DEBUG_PRINT(' ');
    }
    DEBUG_PRINTLN();
}

void setup() {
    Serial.begin(115200);

    pinMode(STEP_RIGHT, OUTPUT);
    pinMode(STEP_LEFT, OUTPUT);
    pinMode(ENABLE_LEFT, OUTPUT);
    pinMode(ENABLE_RIGHT, OUTPUT);
    pinMode(DIR_RIGHT, OUTPUT);
    pinMode(DIR_LEFT, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);

    qtr.setTypeRC();
    qtr.setSensorPins((const uint8_t[]) {41, 49, 33, 51, 31, 53, 50, 52}, SensorCount);
    qtr.setEmitterPin(35);

    sensor0I2C.begin(27, 29);

    DEBUG_PRINTLN("Adafruit VL53L0X test");

    if (!lox0.begin(&sensor0I2C, VL53L0X_I2C_ADDR, Adafruit_VL53L0X::VL53L0X_SENSE_LONG_RANGE)) {
        DEBUG_PRINTLN_F("Failed to boot VL53L0X Sensor 0");
        while (true);
    }

    sensor1I2C.begin(23, 25);
    if (!lox1.begin(&sensor1I2C, VL53L0X_I2C_ADDR, Adafruit_VL53L0X::VL53L0X_SENSE_LONG_RANGE)) {
        DEBUG_PRINTLN(F("Failed to boot VL53L0X Sensor 1"));
        while (true);
    }

    sensor2I2C.begin(17, 16);
    if (!lox2.begin(&sensor2I2C, VL53L0X_I2C_ADDR, Adafruit_VL53L0X::VL53L0X_SENSE_LONG_RANGE)) {
        DEBUG_PRINTLN(F("Failed to boot VL53L0X Sensor 2"));
        while (true);
    }
    lox0.startRangeContinuous(50);
    lox1.startRangeContinuous(50);
    lox2.startRangeContinuous(50);

    CLR(DIR_LEFT_PORT, DIR_LEFT_PIN);
    CLR(DIR_RIGHT_PORT, DIR_RIGHT_PIN);

    CLR(ENABLE_LEFT_PORT, ENABLE_LEFT_PIN);
    CLR(ENABLE_RIGHT_PORT, ENABLE_RIGHT_PIN);

    setupTimer3();

    DEBUG_PRINTLN("Init done!");
}

void loop() {
    readLidarSensors();

    DEBUG_PRINT("S0: ");
    DEBUG_PRINT(sensorData[0]);
    DEBUG_PRINT(" |S1: ");
    DEBUG_PRINT(sensorData[1]);
    DEBUG_PRINT(" |S2: ");
    DEBUG_PRINTLN(sensorData[2]);

    // read calibrated sensor values and obtain a measure of the line position
    // from 0 to 5000 (for a white line, use readLineWhite() instead)
    uint16_t position = qtr.readLineBlack(sensorValues);

    // print the sensor values as numbers from 0 to 1000, where 0 means maximum
    // reflectance and 1000 means minimum reflectance, followed by the line
    // position
    for (unsigned short sensorValue: sensorValues) {
        DEBUG_PRINT(sensorValue);
        DEBUG_PRINT('\t');
    }
    DEBUG_PRINTLN(position);

    if (position < 2500) {
        SET(ENABLE_LEFT_PORT, DIR_LEFT_PIN);
        CLR(ENABLE_RIGHT_PORT, DIR_RIGHT_PIN);
    } else if (position > 4500) {
        CLR(ENABLE_LEFT_PORT, DIR_LEFT_PIN);
        SET(ENABLE_RIGHT_PORT, DIR_RIGHT_PIN);
    } else {
        CLR(ENABLE_LEFT_PORT, DIR_LEFT_PIN);
        CLR(ENABLE_RIGHT_PORT, DIR_RIGHT_PIN);
    }

    delay(50);

    SBI(LED_BUILTIN_PORT, LED_BUILTIN_PIN);

}

DISABLE_WARNING_POP