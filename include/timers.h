// MAI Cup 2022 Senior Team
// Copyright (C) 2021  MAI Robotics
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//
// Created by Julius Gesang on 06.12.21.
//

#ifndef TIMERS_H
#define TIMERS_H

#include <Arduino.h>
#include "macros.h"
#include "config.h"

#define TIMER_RESOLUTION 65536UL

#define TIMER4_CYCLES ((F_CPU / 2000000L) * TIMER4_TIMING)

FORCE_INLINE void setupTimer3() {
    TCCR3B = _BV(WGM33) | _BV(CS30);    // set mode as phase and frequency correct pwm, stop the timer
    TCCR3A = 0;                         // clear control register A (This disconnects OC3A/B/C)
    ICR3 = 8 * STEPPER_TIMING;          // TOP of Timer
    TIMSK3 = _BV(TOIE3);                // Set overflow Interrupt enable
}

FORCE_INLINE void setupTimer4() {
#if TIMER4_CYCLES < TIMER_RESOLUTION
    TCCR4B = _BV(WGM43) | _BV(CS40);    // set mode as phase and frequency correct pwm, stop the timer
    ICR4 = TIMER4_CYCLES;               // TOP of Timer
#elif TIMER4_CYCLES < TIMER_RESOLUTION * 8
    TCCR4B = _BV(WGM43) | _BV(CS41);    // set mode as phase and frequency correct pwm, stop the timer
    ICR4 = TIMER4_CYCLES / 8;               // TOP of Timer
#elif TIMER4_CYCLES < TIMER_RESOLUTION * 64
    TCCR4B = _BV(WGM43) | _BV(CS41) | _BV(CS40);    // set mode as phase and frequency correct pwm, stop the timer
    ICR4 = TIMER4_CYCLES / 64;               // TOP of Timer
#elif TIMER4_CYCLES < TIMER_RESOLUTION * 256
    TCCR4B = _BV(WGM43) | _BV(CS42);    // set mode as phase and frequency correct pwm, stop the timer
    ICR4 = TIMER4_CYCLES / 256;               // TOP of Timer
#elif TIMER4_CYCLES < TIMER_RESOLUTION * 1024
    TCCR4B = _BV(WGM43) | _BV(CS42) | _BV(CS40);    // set mode as phase and frequency correct pwm, stop the timer
    ICR4 = TIMER4_CYCLES / 1024;               // TOP of Timer
#else
    TCCR4B = _BV(WGM43) | _BV(CS42) | _BV(CS40);    // set mode as phase and frequency correct pwm, stop the timer
    ICR4 = TIMER_RESOLUTION - 1;               // TOP of Timer
#endif

    TCCR4A = 0;                         // clear control register A (This disconnects OC4A/B/C)
    TIMSK4 = _BV(TOIE4);                // Set overflow Interrupt enable
}

#endif //TIMERS_H
